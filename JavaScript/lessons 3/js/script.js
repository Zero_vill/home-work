let style = []; 
style[0] = "Jazz";
style[1] = "Blues";

document.write(style + "<br>");

//Добавить два элемента в style масcив c помощью метода push 
style.push("Rock and roll");

document.write(style.join("*") + "<br>");

//Удалить элемент с начала масcива style с помощью метода shift
let styleShiftfirstelement = style.shift();

document.write("Выдвинутое значение:" + styleShiftfirstelement + "<br>");

document.write("Массив после сдвига c применением метода shift:" + style.join("*") + "<br>");

//Добавить два элемента в начало масcива  style с помощью метода unshift
let styleUnshiftfirstelement = style.unshift("Rap", "Peggy");

document.write("Массив после сдвига c применением метода unshift:" + style.join("*") + "<br>");

//Длина массива 
alert("Длина массива " + style.length);

let stylePush = prompt("Добавить элемент в конец масcива:");

stylePush != null ? style.push(stylePush) : style;
//Длина масcива в случае добавления элемента 
alert("Длина массива " + style.length);
//Заменить средний элемент при любой длине масcива 
let musicGanre = prompt("Заменить средний элемент на:");
/*Если длина маcсива не четная то заменяется средний элемент, 
в случае если длина масcива четная то элемент добавляется в середину*/
if (musicGanre != null) {
  if (style.length % 2 != 0) {
    style.splice(Math.floor(style.length / 2), 1, musicGanre);
  }else{
    let styleChoice = prompt("В массиве четное количество элементов - выбрано два средних элемента. Заменить элемент " + style[(style.length / 2) - 1] + " или элемент " + style[(style.length / 2)] + "?");

    if (styleChoice == style[(style.length / 2) - 1]) {
      style.splice((style.length / 2) - 1, 1, musicGanre);
    }else if (styleChoice == style[(style.length / 2)]) {
      style.splice((style.length / 2), 1, musicGanre);
    }else if (styleChoice == null){
      style.splice((style.length / 2), 0, musicGanre);
    }else{
      document.write("Массив был не изменен так как было введено не коректное значение" + "<br>" );
    }
  };
};
document.write(style.join("*") + "</br>");

var arr = new Array(15);

for (var i = 0; i < arr.length; i++) {
	arr[i] = i + 1;
}



document.write("</br>" + arr.join("&#9729;") + "<br/>");

var index = prompt("Введите индекс", "0");

arr.splice(parseInt(index), 1);

arr.reverse();

document.write(arr.join("&#9729;"));
